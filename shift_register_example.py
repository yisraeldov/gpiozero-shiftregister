import gpiozero
import time

ser = gpiozero.DigitalOutputDevice(18) #serial input
oe = gpiozero.DigitalOutputDevice(19, active_high=False) #output enabled
r_clk = gpiozero.DigitalOutputDevice(20) # storage register
sr_clk = gpiozero.DigitalOutputDevice(21) # shift register

oe.on()
sr_clk.blink()


for i in range(8):
    ser.toggle()
    sr_clk.blink(0.1,0.1,1,background=False)
    r_clk.blink(0.1,0.5,1,background=False)

def shift_bits(bits):
    for i in range(8):
        ser.value = bits & 2**i
        sr_clk.blink(0.1,0.1,1,background=False)
    r_clk.blink(0.1,0.5,1,background=False)
        
