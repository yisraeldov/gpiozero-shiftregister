.. gpiozero-shiftregister documentation master file, created by
   sphinx-quickstart on Tue Aug 13 15:51:11 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gpiozero-shiftregister's documentation!
==================================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. autoclass:: gpiozero_shiftregister.ShiftRegisterOutputDevice
   :members:
   :undoc-members:
   :inherited-members:

.. autoclass:: gpiozero_shiftregister.ShiftRegisterFactory
   :members:
   :undoc-members:
   :inherited-members:
   
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
