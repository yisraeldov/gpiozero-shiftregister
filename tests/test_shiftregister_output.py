import pytest
import pprint
import numpy as np
import matplotlib.pyplot as plt
from gpiozero_shiftregister import ShiftRegisterOutputDevice
from gpiozero import DigitalOutputDevice, GPIOPinMissing


def test_serialout_bad_init(mock_factory):
    with pytest.raises(GPIOPinMissing):
        ShiftRegisterOutputDevice()
    with pytest.raises(GPIOPinMissing):
        ShiftRegisterOutputDevice(2)
    with pytest.raises(GPIOPinMissing):
        ShiftRegisterOutputDevice(shift_clock=2)
    with pytest.raises(GPIOPinMissing):
        ShiftRegisterOutputDevice(serial=2)
    with pytest.raises(GPIOPinMissing):
        ShiftRegisterOutputDevice(1, 2)
    with pytest.raises(TypeError):
        ShiftRegisterOutputDevice(a=2, b=3)


def plot_states(states):
    plt.step(
        np.cumsum(list(map(lambda x: x.timestamp, states))),
        list(map(lambda x: int(x.state), states)),
        where="post"
    )


def test_serialout_pins(mock_factory):
    ser = mock_factory.pin(1)
    s_clk = mock_factory.pin(2)
    r_clk = mock_factory.pin(3)
    with ShiftRegisterOutputDevice(1, 2, 3) as shiftregister:
        assert shiftregister.serial_device.pin is ser
        assert isinstance(shiftregister.serial_device, DigitalOutputDevice)

        assert shiftregister.shift_clock_device.pin is s_clk
        assert isinstance(shiftregister.shift_clock_device,
                          DigitalOutputDevice)

        assert shiftregister.register_clock_device.pin is r_clk
        assert isinstance(shiftregister.register_clock_device,
                          DigitalOutputDevice)


def test_serialout_invalid_setvalue(mock_factory):
    with ShiftRegisterOutputDevice(1, 2, 3) as shiftregister:
        with pytest.raises(TypeError):
            shiftregister.value = -1
        with pytest.raises(TypeError):
            shiftregister.value = "5 xxx"
            # TODO check that this is actually the right error message
        with pytest.raises(TypeError):
            shiftregister.value = 10.5
        # TODO check not too long


def test_serialout_setvalue(mock_factory):
    ser = mock_factory.pin(1)
    s_clk = mock_factory.pin(2)
    r_clk = mock_factory.pin(3)
    with ShiftRegisterOutputDevice(1, 2, 3) as shiftregister:

        assert shiftregister.value == 0

        s_clk.clear_states()
        ser.clear_states()
        r_clk.clear_states()

        shiftregister.value = 1
        # assert_states only asserts state CHANGES
        ser.assert_states([
            0,
            1,
            0,
            # 0,
            # 0,
            # 0,
            # 0,
            # 0,
            # 0
        ])
        pprint.pprint(s_clk.states)
        pprint.pprint(ser.states)
        s_clk.assert_states([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1])
        r_clk.assert_states([0, 1, 0])
        assert shiftregister.value == 1
#        assert s_clk.states[1].timestamp < ser.states[1].timestamp
#        assert s_clk.states[1].timestamp < ser.states[1].timestamp
        # TODO test the timing is actually correct

        ser.clear_states()

        shiftregister.value = 5
        ser.assert_states([
            # 0,
            1,
            0,
            1,
            # 0,
            # 0,
            # 0,
            # 0,
            # 0
        ])
        assert shiftregister.value == 5

        ser.clear_states()
        shiftregister.value = 0
        ser.assert_states([1, 0, 1, 0, 1, 0, 1, 0])
        shiftregister.value == 0b10101010

        ser.clear_states()
        shiftregister.value = 0b01010101
        # The state was already 1 from last test
        ser.assert_states([0, 1, 0, 1, 0, 1, 0, 1])
        shiftregister.value == 0b01010101

        ser.clear_states()
        s_clk.clear_states()
        r_clk.clear_states()
        shiftregister.value = 0b00001101
 

        pprint.pprint("s clk")
        pprint.pprint(s_clk.states, width=1)
        pprint.pprint(r_clk.states, width=1)
        pprint.pprint(ser.states, width=1)
        plt.xkcd()
        plot_states(s_clk.states)
#        plot_states(r_clk.states)
        plot_states(ser.states)
        plt.savefig("test.png", format="png")
        ser.assert_states([1, 0, 1, 0, 1])


def test_siftregister_lots_of_bits(mock_factory):
    ser = mock_factory.pin(1)
    s_clk = mock_factory.pin(2)
    r_clk = mock_factory.pin(3)
    with ShiftRegisterOutputDevice(1, 2, 3, bits=20) as shiftregister:

        assert shiftregister.value == 0

        s_clk.clear_states()
        ser.clear_states()
        r_clk.clear_states()

        shiftregister.value = 1
        # assert_states only asserts state CHANGES
        ser.assert_states([
            0,
            1,
            0,
            # 0,
            # 0,
            # 0,
            # 0,
            # 0,
            # 0
        ])
        plt.clf()
        plt.xkcd()
        plot_states(s_clk.states)
#        plot_states(r_clk.states)
        plot_states(ser.states)
        plt.savefig("test_32bit.png", format="png")

    
def test_shiftregister_set_pin(mock_factory):
    with ShiftRegisterOutputDevice(1, 2, 3) as shiftregister:
        shiftregister.value = 0
        shiftregister.set_pin(pin=1, value=1)
        assert shiftregister.value == 0b00000010

        shiftregister.value = 0
        shiftregister.set_pin(pin=2, value=1)
        assert shiftregister.value == 0b00000100

        shiftregister.value = 0b00010001
        shiftregister.set_pin(pin=2, value=1)
        assert shiftregister.value == 0b00010101


def test_shiftregister_unset_pin(mock_factory):
    with ShiftRegisterOutputDevice(1, 2, 3) as shiftregister:
        shiftregister.value = 0
        shiftregister.set_pin(pin=1, value=0)
        assert shiftregister.value == 0b00000000

        shiftregister.value = 1
        shiftregister.set_pin(pin=0, value=0)
        assert shiftregister.value == 0b00000000

        shiftregister.value = 0b00010101
        shiftregister.set_pin(pin=2, value=0)
        assert shiftregister.value == 0b00010001
