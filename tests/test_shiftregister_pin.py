import pytest
from gpiozero_shiftregister import (
    ShiftRegisterOutputDevice,
    ShiftRegisterFactory
)

from gpiozero import Factory, OutputDevice, Motor, DigitalOutputDevice


def test_make_shiftregistor_factory(mock_factory):
    srd = ShiftRegisterOutputDevice(1, 2, 3)
    factory = ShiftRegisterFactory(shift_register=srd)
    assert isinstance(factory, Factory)


def test_make_pin_from_shiftregister(mock_factory):
    srd = ShiftRegisterOutputDevice(1, 2, 3)
    factory = ShiftRegisterFactory(shift_register=srd)
    pin = OutputDevice(1, pin_factory=factory)
    assert isinstance(pin, OutputDevice)
    pin.close()

def test_set_pin_sets_register(mock_factory):
    srd = ShiftRegisterOutputDevice(1, 2, 3)
    factory = ShiftRegisterFactory(shift_register=srd)
    pin = DigitalOutputDevice(1, pin_factory=factory)

    srd.value = 0
    pin.on()
    assert srd.value == 0b00000010

    pin.off()
    assert srd.value == 0

    pin.close()


def test_shift_register_motor(mock_factory):
    srd = ShiftRegisterOutputDevice(1, 2, 3)
    factory = ShiftRegisterFactory(shift_register=srd)
    motor = Motor(0, 1, pwm=False, pin_factory=factory)
    assert srd.value == 0

    motor.forward()
    assert srd.value == 1

    motor.backward()
    assert srd.value == 2

    motor.stop()
    assert srd.value == 0

    motor.value = 1
    assert srd.value == 1

def test_shift_register_2motors(mock_factory):
    srd = ShiftRegisterOutputDevice(1, 2, 3)
    factory = ShiftRegisterFactory(shift_register=srd)
    motor1 = Motor(0, 1, pwm=False, pin_factory=factory)
    motor2 = Motor(2, 3, pwm=False, pin_factory=factory)
    assert srd.value == 0

    motor1.forward()
    assert srd.value == 1

    motor1.backward()
    assert srd.value == 2

    motor1.stop()
    assert srd.value == 0

    motor1.value = 1
    assert srd.value == 1
    motor1.value = 0

    motor2.forward()
    assert srd.value == 4


def test_more_motor(mock_factory):
    pytest.skip("TODO fix these failing tests")

    srd = ShiftRegisterOutputDevice(1, 2, 3)
    factory = ShiftRegisterFactory(shift_register=srd)
    motor = Motor(0, 1, pwm=False, pin_factory=factory)
    assert srd.value == 0

    motor.forward()
    assert motor.value == 1
    assert motor.is_active()

    motor.reverse()
    assert srd.value == 2
    assert motor.value == -1
    assert motor.is_active()


def test_fails_without_shiftregister():
    pytest.skip("TODO")
