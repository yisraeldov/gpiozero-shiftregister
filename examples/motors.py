from gpiozero_shiftregister import(
    ShiftRegisterOutputDevice,
    ShiftRegisterFactory
)

from gpiozero import Motor

sr = ShiftRegisterOutputDevice(18, 21, 20)

factory = ShiftRegisterFactory(sr)

motor = Motor(0, 1, pin_factory=factory, pwm=False)

motor.forward()
motor.backward()
motor.stop()
motor.value = 1
motor.value = -1
motor.value = 0

# not working yet
motor.is_active
motor.value
