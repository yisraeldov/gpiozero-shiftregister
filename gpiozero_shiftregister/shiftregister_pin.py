from gpiozero import Factory, Pin, PinInvalidFunction
from collections import defaultdict
from threading import Lock


class ShiftRegisterFactory(Factory):
    def __init__(self, shift_register):
        super().__init__()
        self._shift_register = shift_register
        self.pins = {}
        self.pin_class = ShiftRegisterPin
        self._reservations = defaultdict(list)
        self._res_lock = Lock()

    def pin(self, spec):
        n = spec
        try:
            pin = self.pins[n]
        except KeyError:
            pin = self.pin_class(self, n)
            self.pins[n] = pin
        return pin


class ShiftRegisterPin(Pin):
    def __init__(self, factory, n):
        self._state = None
        self._factory = factory
        self._shift_register = factory._shift_register
        self.number = n

    def _get_function(self):
        return "output"

    def _set_function(self, value):
        if value != "output":
            raise PinInvalidFunction(
                "Cannot set the function of pin %r to %s" % (self, value))

    def _set_state(self, value):
        self._shift_register.set_pin(self.number, value)
        self._state = bool(value)
